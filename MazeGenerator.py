#Library for generate random number
import random
#Maze Generator
def Maze_Generator(row, col, den):
    maze_array = [[None]*col for i in range(row)]
    for i in range(row):
        for j in range(col):
            if i == 0 or j == 0 or i == (row-1) or j == (col-1):
                maze_array[i][j] = "#"
            else:   
                rnd = random.randint(den, 10)
                if rnd == 10:
                    maze_array[i][j] = "@"
                else:
                    maze_array[i][j] = " "
    rowlen = len(maze_array)
    collen = len(maze_array[0])
    while True:
        try:
            x_pos = random.randint(1, (collen-2))
            y_pos = random.randint(1, (rowlen-2))
            print ("columns i try to access", x_pos,"rows i try to access", y_pos)
            print (collen, rowlen)
            maze_array[y_pos][x_pos] = "P"
            break
        except IndexError:
            print("OUT OF RANGE! This should not happen!")
    return maze_array

def Array_to_maze(array):
    maze = ""
    rowlen = len(array)
    collen = len(array[0])
    for i in range(rowlen):
        for e in range(collen):
            maze += array[i][e]
        maze += "\n"
    return maze

#Wecome Screen
print("   __    __    ____  _  _    __  __    __    ____  ____     ___  ____  _  _  ____  ____    __   ____  _____  ____   \n\
  /__\  (  )  (  _ \( \/ )  (  \/  )  /__\  (_   )( ___)   / __)( ___)( \( )( ___)(  _ \  /__\ (_  _)(  _  )(  _ \  \n\
 /(__)\  )(__  ) _ < \  /    )    (  /(__)\  / /_  )__)   ( (_-. )__)  )  (  )__)  )   / /(__)\  )(   )(_)(  )   /  \n\
(__)(__)(____)(____/ (__)   (_/\/\_)(__)(__)(____)(____)   \___/(____)(_)\_)(____)(_)\_)(__)(__)(__) (_____)(_)\_)  ")
print("Welcome to Alby Maze Generator")
#Insertion of info about the maze size and density
while 1 == 1:
    rowsS = input("Insert the rows of the maze(3 - 50): ")
    rows = int(rowsS)
    if rows >= 3 and int(rows) <= 50:
        break
    print("Input incorrect!")
print("Welcome to Alby Maze Generator")
while 1 == 1:
    columnsS = input("Insert the columns of the maze(3 - 50): ")
    columns = int(columnsS)
    if columns >= 3 and int(columns) <= 50:
        break
    print("Input incorrect!")
while 1 == 1:
    densityS = input("Insert the rows of the maze(3 - 50): ")
    density = int(densityS)
    if density >= 1 and int(density) <= 10:
        break
    print("Input incorrect!")
#Maze Generation
print("Generating Maze")
ma = Maze_Generator(int(rows), int(columns), int(density))
print(Array_to_maze(ma))

        

